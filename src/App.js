/**
 * This application is for job interview only, not for commercial purpose. 
 * Author: Do Phu An
 * Implemented on Wednesday Sept 18, 2019
 */

import React, { Component } from 'react';
import './App.css';
import { Row, Col, Container, Button } from 'react-bootstrap';
import LeaguesInfo from './modules/LeaguesDetails';
import Fixtures from './modules/Fixtures';
class App extends Component {
  constructor(props) {
    super(props);
    // handleClick = this.handleClick.bind(this);
    this.state = {
      handleClick: this.handleClick.bind(this),
      fetchFixtures: this.fetchFixtures.bind(this),
      leaguesInfo: [],
      fixtures: [],
      currentView: "Home",
      selectedLeagueId: 0,
    }
  }

  // This variable is to add a proxy for the local host to fetch data without causing CORS error.
  proxyUrl = "https://cors-anywhere.herokuapp.com/";

  /**
   * Depend on the selected league, get its fixture and display
   * @param {} leaguesCode
   */
  fetchFixtures(leaguesCode) {
    fetch(this.proxyUrl + 'https://www.api-football.com/demo/api/v2/teams/league/' + leaguesCode)
      .then(res => res.json())
      .then((data) => {
        this.setState({ fixtures: data.api.teams })
        console.log(this.state.fixtures);
      })
      .catch(console.log("Cannot fetch data from server"))
  }
  /**
   * Handle click event, change the view to fixture view.
   */
  handleClick(someVar) {
    this.setState({ currentView: "Fixture", selectedLeagueId: someVar });
  }

  render() {
    // console.log(this.state.leaguesInfo);
    return (
      <div className="App">
        <div className="background">
          <Container>
            {/* First row */}
            <Row>
              <Col md="6">
                <Button color="primary" size="lg" block className="mb-2">LIVESTREAM BÓNG ĐÁ ẢO</Button>{' '}
              </Col>
              <Col md="6">
                <Button color="primary" size="lg" block className="mb-2">LIVESTREAM XÓC DĨA, TÀI XỈU</Button>{' '}
              </Col>
            </Row>

            {/* Second row */}
            <Row>
              <Col md="6">
                <Button color="primary" size="lg" block className="mb-2">LIVESTREAM ĐUA CHÓ ẢO</Button>{' '}
              </Col>
              <Col md="6">
                <Button color="primary" size="lg" block className="mb-2">CÁC KÈO E-SPORTS HOT NHẤT</Button>{' '}
              </Col>
            </Row>
          </Container>

          {/* Add the table */}
          <Container className="mt-5">
            <Col md="12">
              <Row>
                <Col md="12" className="table" id="liveBroadCastTableHeader">LỊCH PHÁT SÓNG TRỰC TIẾP</Col>
              </Row>
              {this.state.currentView === "Home" ? <LeaguesInfo handleClick={this.state.handleClick} /> : null}
              {this.state.currentView === "Fixture" ? <Fixtures fixtures={this.state.fetchFixtures} selectedLeagueId={this.state.selectedLeagueId} /> : null}
            </Col>

          </Container>
        </div>
      </div>
    );
  }
}


export default App;
