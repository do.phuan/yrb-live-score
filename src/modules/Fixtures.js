import React from 'react';
import { Row, Col } from 'react-bootstrap';

class Fixtures extends React.Component {
    state = {
        fixtures: [],
    }

    /**
    * Call API and return a json
    */
    proxyUrl = "https://cors-anywhere.herokuapp.com/";
    fetchLeaguesFixture() {
        fetch(this.proxyUrl + 'https://www.api-football.com/demo/api/v2/teams/league/' + this.props.selectedLeagueId)
            .then(res => res.json())
            .then((data) => {
                this.setState({fixtures : data.api.teams})
            })
            .catch(console.log("Cannot fetch data from server"))
    }
    componentDidMount() {
        this.fetchLeaguesFixture();
        console.log(this.state.fixtures);
    }
    render() {
        var displayedTable = this.state.fixtures.map((obj) => <Col md="6" id="tableCell">
            <Row>
                <Col md="12" id="country">{obj.country}</Col>
            </Row>
            <Row>
                <Col md="12" id="club">{obj.name}</Col>
            </Row>
            <Row>
                <Col md="4" id="venue">{obj.venue_name}</Col>
                <Col md="4" id="venue">{obj.venue_address}</Col>
                <Col md="4" id="venue">{obj.venue_city}</Col>
            </Row>
        </Col>);
        return (<Row>{displayedTable}</Row>);
    }
};

export default Fixtures;