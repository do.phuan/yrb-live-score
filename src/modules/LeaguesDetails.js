import React from 'react';
import { Row, Col, Image } from 'react-bootstrap';
import roundedBtn from '../images/roundedPlayBtn.png';

class LeaguesInfo extends React.Component {
    state = {
        leaguesInfo: [],
        handleClickEvent: this.handleClickEvent.bind(this),
        selectedLeagueId: 0
    }
    startYear = "";
    startDate = "";
    /**
    * Call API and return a json
    */
    proxyUrl = "https://cors-anywhere.herokuapp.com/";
    componentDidMount() {
        fetch(this.proxyUrl + 'https://www.api-football.com/demo/api/v2/leagues')
            .then(res => res.json())
            .then((data) => {
                this.setState({leaguesInfo : data.api.leagues})
            })
            .catch(console.log("Cannot fetch data from server"))
    }
    /**
     * Change the view when user click play btn on the table
     */
    handleClickEvent() {
        this.props.handleClick();
    }

    render() {
        var displayedTable = this.state.leaguesInfo.map((obj) => <Col md="6" id="tableCell">
        <Row id="liveStreamTitle">LIVESTREAM BÓNG ĐÁ ẢO</Row>
            <Row>
                <script>
                    var temp = obj.season_start.split("\d{4}-");
                    this.startYear = temp[0].replace("-","");
                    this.startDate = temp[1];
                </script>
                <Col md="2" id="startDateWithRedBox">
                    <Row>
                        <Col md="12" id="dateInRedBox">{obj.season_start.replace(/\d{4}-/g, "")}</Col>
                    </Row>
                    <Row>
                        <Col md="12" id="yearInRedBox">{obj.season_start.replace(/-\d{2}-\d{2}/g,"")}</Col>
                    </Row>
                    {/* <Row>{obj.season_start.replace(/-\d{2}-\d{2}/g,"")}</Row> */}
                </Col>
                <Col md="6">
                    <Row>
                        <Col md="12" id="leaguesTitle">{obj.name}</Col>
                    </Row>
                    <Row>
                        <Col md="4">
                            <Image src={obj.flag} id="flag" onError={e => e.target.style.display = 'none'}/>
                        </Col>
                        <Col md="8" id="countryName">{obj.country}</Col>
                    </Row>
                </Col>
                <Col md="3">
                    <Image src={roundedBtn} onClick={() => this.props.handleClick(obj.league_id)}/>
                </Col>
            </Row>
        </Col>);
        return (<Row>{displayedTable}</Row>);

    }
};

export default LeaguesInfo